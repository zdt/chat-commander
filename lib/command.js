"use strict"

const util = require("util")
const Meta = require("./meta")
const Promise = require("bluebird")

class Command extends Meta {
	constructor(name) {
		super(name)

		this._action = () => {}
	}

	get parse() {
		return this._parse || (query => query)
	}

	set parse(fn) {
		if (!util.isFunction(fn)) throw new Error(`expected function, got ${typeof fn}`)
		this._parse = fn
	}

	get action() {
		return this._action
	}

	set action(fn) {
		if (!util.isFunction(fn)) throw new Error(`expected function, got ${typeof fn}`)
		this._action = fn
	}

	call(request, context) {
		return this.action(request, context)
	}
}

module.exports = Command
