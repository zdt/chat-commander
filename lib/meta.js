"use strict"

class Meta {
	constructor(name) {
		if (name && name.match(/[^\d\w]/)) throw new Error("group name can only consist of alphanumeric signs")
		this._name = name
	}

	get name() {
		return this._name
	}

	get description() {
		return this._description || ""
	}

	set description(value) {
		if (value.indexOf("\n") >= 0) throw new Error("description contains a new line")

		this._description = value
	}

	get help() {
		return this._help || ""
	}

	set help(value) {
		this._help = value
	}

	usage(txt) {
		const [description, help] = txt.split("\n\n")
		this.description = description
		this.help = help
	}
}

module.exports = Meta
