"use strict"

module.exports = command => {
    command.description = "Show help and usage about available commands"
    command.help = "Show help and usage guidelines for all available commands and command groups"
    // TODO: command.arguments
    
    command.action = (params, message, discordie) => {
        const q = ctx.params.command || ""
        const p = q.split(/\s+/)
        
        let message = !q.length? this.$config.help + "\n\n": "" 
        
        // build command path
        let node = this
        let x = null
        while (x = p.shift()) {
            if (node.groups[x]) {
                node = node.groups[x]
                continue
            }
            
            if (node.commands[x]) {
                node = node.commands[x]
                continue
            }
            
            throw new Error(message + "No help for `" + q + "` found!")
        }
        
        // check if we have groups
        if (node.groups && Object.keys(node.groups).length) {
            message += "**Available Groups**\n" + Object.keys(node.groups).map(gn => node.groups[gn]).filter(g => !g.hidden).map(g => {
                return "`" + g.name + "` - " + g.description
            }).join("\n") + "\n\n"
        }
        
        if (node.commands && Object.keys(node.commands).length) {
            message += "**Available Commands**\n"
            
            // check if we have an unnamed command
            if (node.commands[""]) {
                message += "`Directly Callable` - " + node.commands[""].description + "\n\n"
            }
            
            // check if we have commands
            message += Object.keys(node.commands).filter(c => c.length > 0).map(cn => node.commands[cn]).filter(c => !c.hidden && c.name != "").map(c => {
                return "`" + c.name + "` - " + c.description
            }).join("\n") + "\n\n"
        }
        
        // check if this a command
        if (node.call) {
            message += "`" + q + " " + node.usage + "`\n" + node.description + "\n\n" + node.help
        }
        
        return ctx.sendMessage(message)
    }
}