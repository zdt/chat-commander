"use strict"

const Meta = require("./meta")
const Command = require("./command")

class Group extends Meta {
	constructor(name = "", sep = "/") {
		super(name)
		if (sep.match(/\s/)) throw new Error("Whitespace not allowed in the seperator")
		this._separator = sep

		this._command = false
		this._commands = {}
		this._groups = {}
	}

	_buildCommands(prefix) {
		const _p = (prefix || "") + this.name
		const sep = _p.length > 0? this._separator: ""

		const commandList = {}
		Object.keys(this._groups).forEach(groupName => {
			Object.assign(commandList, this._groups[groupName]._buildCommands(_p + this._separator))
		})

		Object.keys(this._commands).forEach(commandName => {
			commandList[_p + sep + commandName] = this._commands[commandName]
		})

		if (this._command) commandList[_p] = this._command

		return commandList
	}

	callable() {
		return this._command || new Command("")
	}

	get commands() {
		return this._commands
	}

	command(name) {
		if (!this._commands[name]) this._commands[name] = new Command(name)
		return this._commands[name]
	}

	get groups() {
		return this._groups
	}

	group(name) {
		if (!this._groups[name]) this._groups[name] = new Group(name)
		return this._groups[name]
	}
}

module.exports = Group
